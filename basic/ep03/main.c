#include <stdio.h>

#define PI 3.1415

int main() {
    double radius = 0;
    printf("Please insert the circle's radius:\n");
    scanf("%lf", &radius);
    double area = PI * radius * radius;
    printf("The circle's area is: %lf\n", area);
    return 0;
}