#include <stdio.h>

int calcFactorial(int n) {
    if (n == 0) {
        return 1;
    }
    return calcFactorial(n - 1) * n;
}

void printFactorialNumbers(int n) {
    printf("Factorial of %d: %d\n", n, calcFactorial(n));
    if (n == 0) {
        return;
    }
    printFactorialNumbers(n - 1);
}

int main() {
    printFactorialNumbers(10);
    return 0;
}