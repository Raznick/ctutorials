#include <stdio.h>

int main() {
    int number = 0, totalNumbers = 0;
    /**
    printf("Enter a number:\n");
    scanf("%d", &number);
    while (number % 2 == 0) {
        totalNumbers++;
        printf("You entered %d even numbers! Enter another one:\n", totalNumbers);
        scanf("%d", &number);
    }
    **/
    do {
        printf("You entered %d even numbers! Enter another one:\n", totalNumbers);
        scanf("%d", &number);
        totalNumbers++;
    } while (number % 2 == 0);
    printf("You entered an odd number!\n");
    return 0;
}

/**
 * while (game_is_running) {
 *      game_logic
 *      game_rendering...
 *      game_is_running = should_game_close()
 * }