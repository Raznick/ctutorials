#include <stdio.h>

int main() {
    int number = 0, totalNumbers = 0;
    printf("Enter a number:\n");
    scanf("%d", &number);
    while (number % 2 == 0) {
        totalNumbers++;
        printf("You entered %d even numbers! Enter another one:\n", totalNumbers);
        scanf("%d", &number);
    }
    printf("You entered an odd number!\n");
    return 0;
}