#include <stdio.h>

int main() {
    // ++i <-> i = i + 1
    // ++i / i++?
    // int i = 0;
    // int j = ++i; -> j = 1, i = 1
    // int i = 0;
    // int j = i++; -> j = 0, i = 1
    for (int i = 1 ; i <= 5 ; ++i) {
        printf("I'm going to print this so many times! This is print number %d\n", i);
    }
    return 0;
}