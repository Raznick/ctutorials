#include <stdio.h>

int main() {
    int rows = 0, value = 1;
    printf("Enter the number of rows:\n");
    scanf("%d", &rows);

    for (int i = 0 ; i < rows ; ++i) {
        // Push the line (3 * (rows - i )) whitespaces in.
        for (int space = 0 ; space < rows - i ; ++space) {
            printf("   ");
        }
        // Calculate the Pascal value, i - the power, j - element index
        for (int j = 0 ; j <= i ; ++j) {
            if (j == 0 | i == 0) {
                value = 1;
            } else {
                value = (value * (i - j + 1)) / j;
            }
            // Will print the value with up to 6 whitespaces before the number.
            printf("%6d", value);
        }
        printf("\n");
    }

    return 0;
}