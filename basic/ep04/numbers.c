#include <stdio.h>

int main() {
    int number = 0;
    printf("Enter a number:\n");
    scanf("%d", &number);

    if (number < 100 || number > 999) {
        printf("I said 3 digits number!\n");
        return 1;
    }

    // 123 -> GOOD, 321 -> BAD, 100 -> BAD, 678 -> GOOD
    int ones = number % 10;
    int tens = (number / 10) % 10;
    int hundreds = number / 100;
    if (hundreds == tens - 1 && tens == ones - 1) {
        printf("Great!\n");
    } else {
        printf("Yikes!\n");
    }

    return 0;
}