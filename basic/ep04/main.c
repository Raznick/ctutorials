#include <stdio.h>

int main() {
    int number = 0;
    printf("Enter a number:\n");
    scanf("%d", &number);

    if (number % 2 == 0) {
        printf("Your number is even!\n");
    } else if (number > 10) {
        printf("Your number is ODD and greater than 10!\n");
    } else {
        printf("Your number is ODD and less than or equal to 10\n");
    }

    return 0;
}