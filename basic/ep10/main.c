#include <stdio.h>

int main() {
	int num = 5;
	int* pointer_to_num = &num;
	int** pointer_to_pointer_to_num = &pointer_to_num;
	printf("p1: %p, p2: %p\n", pointer_to_num, pointer_to_pointer_to_num);
	int* pointer = NULL;
	
	return 0;
}