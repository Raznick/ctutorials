#include <stdio.h>
#include "func.h"

int getNumberFromUser() {
    int num = 0;
    printf("Please, enter a number:\n");
    scanf("%d", &num);
    return num;
}

void printSum(int n1, int n2) {
    printf("%d + %d = %d\n", n1, n2, addNumbers(n1, n2));
}

int addNumbers(int n1, int n2) {
    return n1 + n2;
}

int main() {
    int num1 = getNumberFromUser();
    int num2 = getNumberFromUser();
    printSum(num1, num2);
    return 0;
}