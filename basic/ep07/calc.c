#include <stdio.h>

int main() {
    char operator = 0;
    float num1 = 0, num2 = 0, result;
    
    printf("Enter the first number:\n");
    scanf("%f", &num1);
    printf("Enter the second number:\n");
    scanf("%f", &num2);
    printf("Choose an operator:\n");
    scanf(" %c", &operator);

    switch (operator) {
        case '+':
        result = num1 + num2;
        break;
        case '-':
        result = num1 - num2;
        break;
        case '*':
        result = num1 * num2;
        break;
        case '/':
        if (num2 == 0) {
            printf("You cannot divide by zero! :O\n");
            return 1;
        }
        result = num1 / num2;
        break;
        default:
        printf("What is this operator?! :S\n");
        return 2;
    }

    printf("The result is: %f\n", result);
    
    return 0;
}