#ifndef __MY_LIB__H
#define __MY_LIB__H

int getNumberFromUser();

void printSum(int n1, int n2);

int addNumbers(int n1, int n2);

#endif