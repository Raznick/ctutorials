#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[]) {
	if (argc < 2) {
		printf("Wrong number of arguments!\n");
		return 0;
	}
	if (strcmp(argv[1], "add") == 0) {
		if (argc != 4) {
			printf("Wrong number of arguments!\n");
			return 0;
		}
		int num1 = atoi(argv[2]);
		int num2 = atoi(argv[3]);
		printf("%d + %d = %d\n", num1, num2, num1 + num2);
	} else if (strcmp(argv[1], "sub") == 0) {
		if (argc != 4) {
			printf("Wrong number of arguments!\n");
			return 0;
		}
		int num1 = atoi(argv[2]);
		int num2 = atoi(argv[3]);
		printf("%d - %d = %d\n", num1, num2, num1 - num2);
	} else if (strcmp(argv[1], "help") == 0) {
		printf("Use: add <num1> <num2> OR sub <num1> <num2>\n");
	}
	
	return 0;
}