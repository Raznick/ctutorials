#include <stdio.h>
#include <stdlib.h>

float calc_avg(int* grades, int num_of_grades) {
	int sum = 0;
	for (int i = 0 ; i < num_of_grades ; ++i) {
		sum += *(grades + i);
	}
	return (float)sum / num_of_grades;
}

int main() {
	int num_of_students = 0;
	printf("Enter number of students:\n");
	scanf("%d", &num_of_students);
	int* grades = (int*)malloc(num_of_students * sizeof(int));
	if (grades == NULL) {
		printf("There was an error allocating memory for grades!\n");
		return -1;
	}

	for (int i = 0; i < num_of_students ; ++i) {
		printf("Enter grade for student number %d:\n", i + 1);
		scanf("%d", grades + i);
	}
	printf("The avg of the class is: %f\n", calc_avg(grades, num_of_students));

	free(grades);

	return 0;
}
