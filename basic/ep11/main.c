#include <stdio.h>

#define NUM_OF_GRADES 5

float calc_avg(int grades[NUM_OF_GRADES]) {
	int sum = 0;
	for (int i = 0 ; i < NUM_OF_GRADES ; ++i) {
		sum += grades[i];
	}
	return (float)sum / NUM_OF_GRADES;
}

int main() {
	int grades[NUM_OF_GRADES] = { 100, 100, 100, 100, 100 };
	printf("The average is = %f\n", calc_avg(grades));

	char str[6] = { 'H', 'e', 'l', 'l', 'o', 0 };
	printf("The string is: %s\n", str);

	return 0;
}