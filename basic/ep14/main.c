#include <stdio.h>
#include <string.h>

typedef union {
	struct {
		unsigned char byte1;
		unsigned char byte2;
		unsigned char byte3;
		unsigned char byte4;
	} bytes;
	unsigned int dword;
} MemoryChunk;

int main() {
	MemoryChunk chunk;
	chunk.dword = 4278255615;
	chunk.bytes.byte3 = 255;

	printf("The number is: %u\n", chunk.dword);

	return 0;
}